package com.example.tp4

import android.arch.persistence.room.*
import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.ProgressBar

@Entity
data class Breed(
    @PrimaryKey var id:String,
    var name:String,
    var origin:String)

@Dao
interface CatDAO {
    @Query("Select * from Breed")
    fun getAllBreeds():Array<Breed>

    @Insert
    fun insert(breed:Breed)

}

@Database(entities = [Breed::class], version = 1)
abstract class CatDatabase : RoomDatabase() {

    abstract fun getDAO():CatDAO

    companion object {
        private var INSTANCE:CatDatabase? = null


        @JvmStatic
        fun getDatabase(context:Context): CatDatabase{
            if (INSTANCE == null) {

                var INSTANCE = Room.databaseBuilder(context, CatDatabase::class.java,"cats.db").allowMainThreadQueries().build()
            }
            return INSTANCE!!
        }
    }
}

//data class ImageViewHolder(val cardview:CardView, val imageview:ImageView, val bar:ProgressBar):RecyclerView.ViewHolder()

//class ImageAdapter(val urls:List<String>, ):RecyclerView.Adapter<ImageViewHolder>(){
//
//}