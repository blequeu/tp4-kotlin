package com.example.tp4

import android.util.JsonReader
import java.io.BufferedInputStream
import java.io.Reader
import java.lang.Exception
import java.net.URL
import java.util.stream.Stream
import javax.net.ssl.HttpsURLConnection

class CatService(val dao:CatDAO){


    fun loadBreeds():Boolean{
        val url = URL("https://api.thecatapi.com/v1/breeds")

        try {
            with(url.openConnection() as HttpsURLConnection){
               setRequestProperty("x-api-key", "7143a2a6-2aa6-419b-a95f-6a9bcc31a3d6")
                JsonReader(BufferedInputStream(this.inputStream) as Reader).use {
                    it.beginArray()
                    while (it.hasNext()){
                        it.beginObject()
                        while (it.hasNext()){
                            var br = Breed("", "", "")
                            when (it.nextName()) {
                                "id" -> {
                                    br.id = it.nextString()
                                }
                                "name" -> {
                                    br.name = it.nextString()
                                }
                                "origin" -> {
                                   br.origin = it.nextString()
                                }
                                else -> {
                                    it.skipValue()
                                }
                            }
                            dao.insert(br)
                        }
                    }
                }
            }

            return true
        }
        catch(e:Exception) {
            return false
        }
    }

    fun loadImages(breed:Breed):List<String>{
        val url = URL("https://api.thecatapi.com/v1/images/search?breed_id=${breed.id}&limit=10")
        var lesurls = arrayListOf<String>()
        try {
            with(url.openConnection() as HttpsURLConnection){
                setRequestProperty("x-api-key", "7143a2a6-2aa6-419b-a95f-6a9bcc31a3d6")
                JsonReader(BufferedInputStream(this.inputStream) as Reader).use {
                    it.beginArray()
                    while (it.hasNext()){
                        it.beginObject()
                        while (it.hasNext()){
                            when(it.nextName()){
                                "url" -> {
                                    lesurls.add(it.nextString())
                                }
                                else -> {
                                    it.skipValue()
                                }
                            }
                        }
                    }
                }
            }
            return lesurls
        }
        catch (e:Exception){
            return lesurls
        }
    }

}