package com.example.tp4

import android.content.Context
import android.graphics.Bitmap
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val spinnerbreed = findViewById<Spinner>(R.id.spn)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<RecyclerView>(R.id.recyl).layoutManager = LinearLayoutManager(this)

        var bdd = CatDatabase.getDatabase(this@MainActivity)
        var dao = bdd.getDAO()
        var service = CatService(dao)

        var lesraces = dao.getAllBreeds()
        if (!lesraces.none()) {

            spinnerbreed.adapter = ArrayAdapter(
                this@MainActivity,
                android.R.layout.simple_dropdown_item_1line,
                lesraces
            )
        }
        else {

            LoadBreedsTask().execute()
        }

        spinnerbreed.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val toast = Toast.makeText(this@MainActivity, spinnerbreed.selectedItem.toString(), Toast.LENGTH_SHORT)
                toast.show()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

    }



    inner class LoadBreedsTask : AsyncTask<Void, Void, Boolean>() {
        val catdao = CatDatabase.getDatabase(this@MainActivity).getDAO()
        val catservice = CatService(catdao)
        val progress = findViewById<ProgressBar>(R.id.prg)
        val spinner = findViewById<Spinner>(R.id.spn)

        override fun onPreExecute() {
            super.onPreExecute()
            progress.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            return catservice.loadBreeds()

        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            progress.visibility = View.GONE
            if (result == true){
                spn.adapter = ArrayAdapter(
                    this@MainActivity,
                    android.R.layout.simple_dropdown_item_1line,
                    catdao.getAllBreeds())
            }
        }


    }
}
